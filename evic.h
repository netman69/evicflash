#ifndef __EVIC_H__
#define __EVIC_H__

#define VENDOR_ID 0x0416
#define PRODUCT_ID 0x5020

#define DATAFLASH_SIZE 2048

#define CMD_DATAFLASH_READ 0x35
#define CMD_DATAFLASH_WRITE 0x53
#define CMD_APROM_WRITE 0xC3
#define CMD_RESET 0xB4

extern void evic_start(void);
extern void evic_end(void);
extern void evic_end(void);
extern int evic_read(void *buf, unsigned int len); /* warning, might write 63 bytes past len */
extern int evic_write(void *buf, unsigned int len);
extern int evic_cmd(unsigned char cmd, unsigned int a0, unsigned int a1);

#endif /* __EVIC_H__ */

