#ifndef __FILE_H__
#define __FILE_H__

typedef struct {
	unsigned char *data;
	unsigned int len;
} file_t;

extern void file_read(file_t *file, char *filename);
extern void file_write(file_t *file, char *filename);
extern void file_allocate(file_t *file, unsigned int size);
extern void file_free(file_t *file);
extern void file_convert(file_t *file);

#endif /* __FILE_H__ */
