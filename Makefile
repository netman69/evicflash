OBJS=file.o evic.o main.o
LDFLAGS=-lhidapi-libusb -lusb-1.0

all: evicflash

evicflash: $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) -o $@

# standalone convert utility, in case you want it
evic_convert:
	$(CC) $(LDFLAGS) $(CFLAGS) convert.c -o $@

clean:
	rm -f $(OBJS) evicflash evic_convert
