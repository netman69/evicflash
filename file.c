#include "file.h"

#include <stdlib.h>
#include <malloc.h>
#include <stdio.h>

void file_read(file_t *file, char *filename) {
	FILE *f;
	if ((f = fopen(filename, "r")) == NULL) {
		fprintf(stderr, "ERROR: can't open file '%s'\n", filename);
		exit(1);
	}
	fseek(f, 0, SEEK_END);
	file_allocate(file, ftell(f));
	fseek(f, 0, SEEK_SET);
	if (fread(file->data, 1, file->len, f) != file->len) {
		fprintf(stderr, "ERROR: can't read file '%s'\n", filename);
		exit(1);
	}
	fclose(f);
}

void file_write(file_t *file, char *filename) {
	FILE *f;
	if ((f = fopen(filename, "w")) == NULL) {
		fprintf(stderr, "ERROR: can't open file '%s'\n", filename);
		exit(1);
	}
	if (fwrite(file->data, 1, file->len, f) != file->len) {
		fprintf(stderr, "ERROR: can't write file '%s'\n", filename);
		exit(1);
	}
	fclose(f);
}

void file_allocate(file_t *file, unsigned int size) {
	file->len = size;
	file->data = malloc(size + 64);
	/* the +64 keep some headroom for my sloppy coding style */
	/* evic_read() and evic_write(), could, I think, in some circumstances */
	/* being windows OS, read or write up to 63 bytes past requested bounds */
	if (file->data == NULL) {
		fprintf(stderr, "ERROR: malloc failed\n");
		exit(1);
	}
	file->data[size] = 0;
}

void file_free(file_t *file) {
	free(file->data);
}

void file_convert(file_t *file) {
	int i;
	for (i = 0; i < file->len; ++i)
		file->data[i] = (file->data[i] ^ (file->len + 408376 + i - file->len / 408376)) & 0xFF;
}
