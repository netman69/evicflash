#include "evic.h"

#include <hidapi/hidapi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

hid_device *handle = NULL;

void evic_start(void) {
	hid_init();
	handle = hid_open(VENDOR_ID, PRODUCT_ID, NULL);
	if (handle == NULL) {
		hid_exit();
		fprintf(stderr, "Failed to open or find device.\n");
		exit(1);
	}
	atexit(&evic_end);
		
	{
		#define INFO_STR_MAX 255
		wchar_t wstr[INFO_STR_MAX];
		hid_get_manufacturer_string(handle, wstr, INFO_STR_MAX);
		fprintf(stderr, "Manufacturer: %ls\n", wstr);
		hid_get_product_string(handle, wstr, INFO_STR_MAX);
		fprintf(stderr, "Product: %ls\n", wstr);
		hid_get_serial_number_string(handle, wstr, INFO_STR_MAX);
		fprintf(stderr, "Serial: %ls\n", wstr);
	}
}

void evic_end(void) {
	if (handle != NULL) {
		hid_close(handle);
		hid_exit();
		handle = NULL;
	}
}

int evic_read(void *buf, unsigned int len) {
	int done = 0;
	while (len) {
		int want = (len < 64) ? len : 64;
		int got = hid_read(handle, ((char *) buf) + done, want);
		done += got;
		len -= got;
		if (got < want) {
			fprintf(stderr, "ERROR: failed talking to device\n");
			exit(1);
		}
	}
	return done;
}

int evic_write(void *buf, unsigned int len) {
	int done = 0;
	char data[65];
	data[0] = 0;
	while (len) {
		int want = (len < 64) ? len : 64;
		memcpy(data + 1, ((char *) buf) + done, want);
		int got = hid_write(handle, data, want + 1) - 1;
		done += got;
		len -= got;
		if (got < want) {
			fprintf(stderr, "ERROR: failed talking to device\n");
			exit(1);
		}
	}
	return done;
}

int evic_cmd(unsigned char cmd, unsigned int a0, unsigned int a1) {
	int r;
	struct {
		unsigned char cmd, len;
		unsigned int a0, a1;
		unsigned int sig;
		unsigned int sum;
	} __attribute__((packed, aligned(1))) packet = {
		cmd, 14, a0, a1, 0x43444948, 0
	};
	#define intsum(n) ((n & 0xFF) + ((n >> 8) & 0xFF) + ((n >> 16) & 0xFF) + ((n >> 24) & 0xFF))
	packet.sum = cmd + 14 + intsum(a0) + intsum(a1) + intsum(packet.sig);
	return evic_write((unsigned char *) &packet, 18);
}
