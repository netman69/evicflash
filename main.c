#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "file.h"
#include "evic.h"

void dataflash_verify(file_t *file) {
	int i, sum;
	for (sum = 0, i = 4; i < file->len - 4; ++i)
		sum += file->data[i];
	if (sum != *(unsigned int *) file->data) {
		fprintf(stderr, "ERROR: dataflash checksum failed\n");
		exit(1);
	}
	fprintf(stderr, "Dataflash integrity OK\n");
}

void dataflash_correct(file_t *file) {
	int i, sum;
	for (sum = 0, i = 4; i < file->len - 4; ++i)
		sum += file->data[i];
	(*(unsigned int *) file->data) = sum;
}

void dataflash_dump(file_t *file) {
	fprintf(stderr, "Reading dataflash....\n");
	file_allocate(file, DATAFLASH_SIZE);
	evic_cmd(CMD_DATAFLASH_READ, 0, DATAFLASH_SIZE);
	if (evic_read(file->data, DATAFLASH_SIZE) < DATAFLASH_SIZE) {
		fprintf(stderr, "ERROR: failed to read dataflash\n");
		exit(1);
	}
	dataflash_verify(file);
}

void dataflash_write(file_t *file, int force) {
	fprintf(stderr, "Writing dataflash...\n");
	if (force)
		dataflash_correct(file);
	else dataflash_verify(file);
	if (file->len != DATAFLASH_SIZE) {
		fprintf(stderr, "ERROR: dataflash size incorrect\n");
		exit(1);
	}
	evic_cmd(CMD_DATAFLASH_WRITE, 0, DATAFLASH_SIZE);
	if (evic_write(file->data, DATAFLASH_SIZE) < DATAFLASH_SIZE) {
		fprintf(stderr, "ERROR: failed to write dataflash\n");
		exit(1);
	}
}

void aprom_write(file_t *file, int force, int convert) {
	if (convert)
		file_convert(file);
	/*if (!force) 
		TODO: verify firmware image
	*/
	if (file->len > 60 * 1024) { /* TODO find what maximum sane size is */
		fprintf(stderr, "ERROR: APROM image too big\n");
		exit(1);
	}

	evic_start();
	
	{ /* check if we're in LDROM, go there if necessary */
		file_t df;
		dataflash_dump(&df);
		if (df.data[0x104] != 0) { /* firmware version = 0 in LDROM */
			df.data[0x0D] |= 1; /* LDROM boot flag */
			usleep(100000); /* fails without */
			dataflash_write(&df, 1);
			fprintf(stderr, "Booting to LDROM...\n");
			evic_cmd(CMD_RESET, 0, 0);
			evic_end();
			sleep(2);
			evic_start();
		}
		file_free(&df);
	}
		
	fprintf(stderr, "Writing APROM...\n");
	evic_cmd(CMD_APROM_WRITE, 0, file->len);
	if (evic_write(file->data, file->len) < file->len) {
		fprintf(stderr, "ERROR: failed to write APROM\n");
		exit(1);
	}
	evic_end();
}

int main(int argc, char *argv[]) {
	int i, j;
	file_t file;
	char *prog = argv[0];

	/* parse options */
	int f = 0, u = 0;
	for (i = 1; i < argc; ++i) {
		if (*argv[i] == '-') {
			while (*(++(argv[i]))) {
				switch (*argv[i]) {
					case 'f': ++f; break;
					case 'u': ++u; break;
					case 'o': break; /* ignored to fit in with evic-sdk makefiles */
					default:
						fprintf(stderr, "ERROR: unrecognized argument\n");
						return 1;
				}
			}
			--argc;
			for (j = i--; j < argc; ++j)
				argv[j] = argv[j + 1];
		}
	}

	if (argc >= 3 && strcmp(argv[1], "convert") == 0) {
		file_read(&file, argv[2]);
		file_convert(&file);
		if (argc == 3)
			fwrite(file.data, 1, file.len, stdout);
		else file_write(&file, argv[3]);
		file_free(&file);
		return 0;
	}

	if (argc >= 2 && strcmp(argv[1], "dfread") == 0) {
		evic_start();
		dataflash_dump(&file);
		evic_end();
		if (argc == 2)
			fwrite(file.data, 1, file.len, stdout);
		else file_write(&file, argv[2]);
		file_free(&file);
		return 0;
	}

	if (argc >= 3 && strcmp(argv[1], "dfwrite") == 0) {
		file_read(&file, argv[2]);
		evic_start();
		dataflash_write(&file, f);
		evic_end();
		file_free(&file);
		return 0;
	}
	
	if (argc >= 3 && strcmp(argv[1], "write") == 0) {
		file_read(&file, argv[2]);
		aprom_write(&file, f, !u);
		file_free(&file);
		return 0;
	}
	
	if (argc >= 2 && strcmp(argv[1], "reset") == 0) {
		evic_start();
		fprintf(stderr, "Restarting device...\n");
		evic_cmd(CMD_RESET, 0, 0);
		evic_end();
		return 0;
	}
	
	fprintf(stderr, "usage:\n"
		" (de)crypt firmware:  %s convert <infile> [<outfile>]\n"
		" dump dataflash:      %s dfread [<outfile>]\n"
		" write dataflash:     %s dfwrite [-f] <infile>\n"
		"    -f forces correcting dataflash checksum\n"
		" write firmware:      %s write [-u] [-f] <infile>\n"
		"    -u is for uploading unencrypted firmware\n"
		"    -f turns off verifications\n"
		"restart device:       %s reset\n"
		, prog, prog, prog, prog, prog);

	return 0;
}
