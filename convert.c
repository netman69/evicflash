/* standalone firmware convert script */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	int i, j, len;
	FILE *in, *out = stdout;
	char buf[4096];
	
	if (argc != 2) {
		fprintf(stderr, "usage: %s <infile> > output\n", argv[0]);
		return 1;
	}
	in = fopen(argv[1], "r");
	if (in == NULL) {
		fprintf(stderr, "error: can't open file '%s'\n", argv[1]);
		return 1;		
	}
	fseek(in, 0, SEEK_END);
	len = ftell(in);
	fseek(in, 0, SEEK_SET);
	for (i = 0; i < len; i += j) {
		int l = fread(buf, 1, sizeof(buf), in);
		for (j = 0; j < l; ++j)
			buf[j] = (buf[j] ^ (len + 408376 + i + j - len / 408376)) & 0xFF;
		fwrite(buf, 1, l, out);
	}
	return 0;
}
